__all__=["AbstractTransformer"]
__author__ = "Srijan Sharma"

from xpresso.ai.core.data.pipeline.abstract_ds_pipeline_component import AbstractDSPipelineComponent
from abc import  abstractmethod

class AbstractTransformer(AbstractDSPipelineComponent):
    """
    Represents a Transformer Component
    """
    def  __init__(self,data, state, results, paused):
        AbstractDSPipelineComponent.__init__(self,data, state, results, paused)

    def start(self):
        """
        Entry point for the Component
        Returns:

        """
        self.transform()
        return

    @abstractmethod
    def transform(self,inputs=None):
        """
        Transforms the data
        Returns:

        """
        pass